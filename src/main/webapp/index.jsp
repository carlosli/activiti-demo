<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>

<html>
<head>
    <title>登录页</title>
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
    <%--<link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">--%>
    <script src="/js/jquery.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <style type="text/css">
        /* Space out content a bit */
        body {
            padding-top: 20px;
            padding-bottom: 20px;
        }

        /* Everything but the jumbotron gets side spacing for mobile first views */
        .header,
        .marketing,
        .footer {
            padding-right: 15px;
            padding-left: 15px;
        }

        /* Custom page header */
        .header {
            border-bottom: 1px solid #e5e5e5;
        }

        /* Make the masthead heading the same height as the navigation */
        .header h3 {
            padding-bottom: 19px;
            margin-top: 0;
            margin-bottom: 0;
            line-height: 40px;
        }

        /* Custom page footer */
        .footer {
            padding-top: 19px;
            color: #777;
            border-top: 1px solid #e5e5e5;
        }

        /* Customize container */
        @media (min-width: 768px) {
            .container {
                max-width: 730px;
            }
        }

        .container-narrow > hr {
            margin: 30px 0;
        }

        /* Main marketing message and sign up button */
        .jumbotron {
            text-align: center;
            border-bottom: 1px solid #e5e5e5;
        }

        .jumbotron .btn {
            padding: 14px 24px;
            font-size: 21px;
        }

        /* Supporting marketing content */
        .marketing {
            margin: 40px 0;
        }

        .marketing p + h4 {
            margin-top: 28px;
        }

        /* Responsive: Portrait tablets and up */
        @media screen and (min-width: 768px) {
            /* Remove the padding we set earlier */
            .header,
            .marketing,
            .footer {
                padding-right: 0;
                padding-left: 0;
            }

            /* Space out the masthead */
            .header {
                margin-bottom: 30px;
            }

            /* Remove the bottom border on the jumbotron for visual effect */
            .jumbotron {
                border-bottom: 0;
            }
        }
    </style>

</head>

<body>
<jsp:include page="/common/top.jsp"></jsp:include>

<div class="jumbotron">
    <h1>一个<br/>Activiti<br/>流程引擎<br/>DEMO!</h1>

    <p class="lead">@carlosli</p>

    <p><a class="btn btn-lg btn-success" href="/login/login" role="button">登录</a></p>
</div>


<div class="container">

    <div class="row marketing">
        <div class="col-lg-6">
            <h2>简介</h2>

            <p>采用activiti作为核心流程引擎编写demo，现阶段表单采用外置表单模式</p>

            <h2>技术</h2>

            <p>Activiti 5, Spring 3, Mybatis, Bootstrap, mysql</p>
        </div>

        <div class="col-lg-6">
            <h2>联系方式</h2>

            <p>email：</p>

            <h2>备注</h2>
            <p></p>
        </div>
    </div>

    <hr>

    <footer>
        <p>© carlosli 2014</p>
    </footer>
</div>

</body>
</html>
