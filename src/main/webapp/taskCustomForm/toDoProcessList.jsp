<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html>
<head>
    <title>待办任务列表-自定义表单</title>
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="/js/jquery.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        $(function () {
            $("#submit-form").click(function() {
                //提交iframe嵌入的表单
                var $form = $("form",window.frames["process-content-iframe"].document);
                $.ajax({
                    cache: true,
                    type: "POST",
                    url: $form.attr("action"),//提交的URL
                    data: $form.serialize(), // 要提交的表单,必须使用name属性
                    async: false,
                    success: function (data) {
                        //刷新页面
                        location.reload();
                    },
                    error: function (request) {
                        alert("出错了");
                        location.reload();
                    }
                });
            });
            $("[name='dealProcess']").click(function () {
                var $ele = $(this);
                var taskId = $ele.attr("taskId");
                var taskDefinitionKey = $ele.attr("taskDefinitionKey");
                var processInstanceId = $ele.attr("processInstanceId");
                if (taskDefinitionKey=='managerApprove'){
                    $(".modal-body").height("410px");
                    $("#myModalLabel").html("请假-自定义表单");
                }else if(taskDefinitionKey=='modifyApply') {
                    $(".modal-body").height("410px");
                    $("#myModalLabel").html("修改请假表单-自定义表单");
//                    window.location.href='/taskCustomForm/getTaskForm/'+taskDefinitionKey+'/'+taskId+'/'+processInstanceId;
                }else if(taskDefinitionKey=='reportBack') {
                    $(".modal-body").height("310px");
                    $("#myModalLabel").html("销假-自定义表单");
                }else{
                    alert("!=");
                }
                //嵌入表单页面
                $("#process-content-iframe").attr("src",'/taskCustomForm/getTaskForm/'+taskDefinitionKey+'/'+taskId+'/'+processInstanceId);
            });
        });
    </script>
</head>

<body>
<jsp:include page="/common/top.jsp"></jsp:include>
<div class="container">
<h1>待办任务列表-自定义表单</h1>
<br>
<table class="table table-striped">
    <tr>
        <th> 任务id</th>
        <th> 流程名称</th>
        <th> 任务名称</th>
        <th> 发起人</th>
        <th> 处理人</th>
        <th> 操作</th>
    </tr>


    <c:forEach items="${tasks}" var="task">
        <tr>
            <td>${task.id}</td>
            <td>
                <c:forEach items="${processDefinitions}" var="entry">
                    <c:if test="${entry.key eq task.processInstanceId}">
                        <c:out value="${entry.value.name}"/>
                    </c:if>
                </c:forEach>
            </td>
            <td>${task.name}</td>
            <td>
                <c:forEach items="${historicProcessInstances}" var="entry">
                    <c:if test="${entry.key eq task.processInstanceId}">
                        <c:out value="${entry.value.startUserId}"/>
                    </c:if>
                </c:forEach>
            </td>
            <td>${task.assignee}</td>
            <td>
                <c:if test="${task.assignee == sessionScope.user.id}">
                    <a class="btn btn-default" href="#" taskId="${task.id}" taskDefinitionKey="${task.taskDefinitionKey}" processInstanceId="${task.processInstanceId}" name="dealProcess" data-toggle="modal" data-target="#process-dialog">处理</a></td>
                </c:if>
                <c:if test="${task.assignee != sessionScope.user.id}">
                    <a class="btn btn-default" href="/taskCustomForm/claimProcess?taskId=${task.id}">签收</a>
                </c:if>
            </td>
        </tr>
    </c:forEach>
</table>


<div class="modal fade" id="process-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" id="process-title">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title" id="myModalLabel">Modal title</h2>
            </div>
            <div class="modal-body" id="process-content">
                <iframe id="process-content-iframe" style="width: 100%;height: 100%;" frameborder="no">


                </iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="submit" class="btn btn-primary" id="submit-form">提交</button>
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>
