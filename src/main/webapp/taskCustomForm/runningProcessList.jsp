<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html>
<head>
    <title>已办任务列表-自定义表单</title>
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="/js/jquery.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[name='showImage']").click(function () {
                var processInstanceId = $(this).attr("processInstanceId");
                $("#taskImageLocation").html("<img src=/taskCustomForm/image/"+processInstanceId+">");
            });
            $("[name='showHistory']").click(function () {
                var processInstanceId = $(this).attr("processInstanceId");
                $("#taskHistoryLocation").html("<iframe style='border: 0;width: 100%;' src=/taskCustomForm/runningProcessHistory/"+processInstanceId+" />");
            });
        });
    </script>
</head>

<body>
<jsp:include page="/common/top.jsp"></jsp:include>
<div class="container">
<h1>已办流程列表-自定义表单</h1>
<br>
<table class="table table-striped">
    <tr>
        <th>流程实例ID</th>
        <th>流程名称</th>
        <th>当前节点</th>
        <th>是否挂起</th>
        <th>查看</th>
    </tr>


    <c:forEach items="${runningProcessList}" var="p">
        <tr>
            <td>${p.processInstanceId }</td>
            <td>
                <c:forEach items="${processDefinitions}" var="entry">
                    <c:if test="${entry.key eq p.processDefinitionId}">
                        <c:out value="${entry.value.name}"/>
                    </c:if>
                </c:forEach>
            </td>
            <td>
                <a class="btn btn-default" processInstanceId="${p.processInstanceId}" name="showImage" data-toggle="modal" data-target="#imageDialog">显示当前流程位置</a>
            </td>
            <td>${p.suspended }</td>
            <td>
                <%--<a class="btn btn-default" processInstanceId="${p.processInstanceId}" name="showHistory" data-toggle="modal" data-target="#historyDialog">历史</a>--%>
                <a href="/taskCustomForm/runningProcessHistory/${p.processInstanceId}">历史</a>
            </td>
        </tr>
    </c:forEach>

</table>


<%--用于显示当前节点图片--%>
<div class="modal fade bs-example-modal-sm" id="imageDialog" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="mySmallModalLabel">流程图</h4>
            </div>
            <div class="modal-body" id="taskImageLocation">
            </div>
        </div>
    </div>
</div>
<%--用于显示流程历史记录--%>
<div class="modal fade bs-example-modal-sm" id="historyDialog" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" >历史记录</h4>
            </div>
            <div class="modal-body" id="taskHistoryLocation">
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>
