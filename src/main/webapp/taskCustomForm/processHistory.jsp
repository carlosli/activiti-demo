<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>

<html>
<head>
    <title>流程详情页-自定义表单</title>
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="/js/jquery.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container">
<h1>流程历史详情列表-自定义表单</h1>
<br>

<table class="table table-striped">
    <tr>
        <th> 任务id</th>
        <th> 任务名称</th>
        <th> 开始时间</th>
        <th> 结束时间</th>
        <th> 负责人</th>
        <th> 处理结果</th>
    </tr>
    <c:forEach items="${historicTasks}" var="task">
        <tr>
            <td>${task.id}</td>
            <td>${task.name}</td>
            <td><fmt:formatDate value="${task.startTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
            <td><fmt:formatDate value="${task.endTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
            <td>${task.assignee}</td>
            <td>${task.deleteReason}</td>
        </tr>
    </c:forEach>

</table>

</div>
</body>
</html>
