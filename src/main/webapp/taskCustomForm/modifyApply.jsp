<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>

<html>
<head>
    <title>请假-自定义表单</title>
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="/js/jquery.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <link href="/bootstrap/css/datepicker3.css" rel="stylesheet">
    <script src="/bootstrap/js/bootstrap-datepicker.js"></script>
    <script src="/bootstrap/js/bootstrap-datepicker.zh-CN.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[time='true']").datepicker({
                format: "yyyy-mm-dd",
                language: "zh-CN",
                clearBtn: true,
                todayHighlight: true,
                autoclose: true,
                todayBtn: "linked"
            });
        });
        $(function () {
            $("[name='showImage']").click(function () {
                var processInstanceId = $(this).attr("processInstanceId");
                $("#taskImageLocation").html("<img src=/task/image/"+processInstanceId+">");
            });
        });
    </script>
</head>

<body style="padding-top: 0;">
    <form action="/taskCustomForm/completeTaskModifyApply" method="post">
    <input type="hidden" name="taskId" value="${taskId}">
    <input type="hidden" name="id" value="${leave.id}">
        <div class="alert alert-danger">
            <strong>驳回</strong>申请<br>
            审批意见: ${leave.rejectReason}
        </div>
        <table class="table table-striped">
            <tr>
                <td>请假类型:</td>
                <td>
                    <select name="leaveType" class="form-control">
                        <option value="1" <c:if test="${leave.leaveType == '1'}">selected</c:if>>病假</option>
                        <option value="2" <c:if test="${leave.leaveType == '2'}">selected</c:if>>事假</option>
                        <option value="3" <c:if test="${leave.leaveType == '3'}">selected</c:if>>婚假</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>开始时间:</td>
                <td>
                    <input type="text" name="startTime" class="form-control" value="<fmt:formatDate value="${leave.startTime}" pattern="yyyy-MM-dd"/>" time="true" readonly/></td>
            </tr>
            <tr>
                <td>结束时间:</td>
                <td><input type="text" name="endTime" class="form-control" value="<fmt:formatDate value="${leave.endTime}" pattern="yyyy-MM-dd"/>" time="true" readonly/></td>
            </tr>
            <tr>
                <td>原因:</td>
                <td>
                    <textarea name="reason" class="form-control" rows="3">${leave.reason}</textarea>
                </td>
            </tr>
            <tr>
                <td>是否继续申请:</td>
                <td>
                    <select name="reApply" class="form-control">
                        <option value='true'>继续申请</option>
                        <option value='false'>取消申请</option>
                    </select>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
