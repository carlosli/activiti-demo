<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html>
<head>
    <title>请假-自定义表单</title>
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="/js/jquery.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <link href="/bootstrap/css/datepicker3.css" rel="stylesheet">
    <script src="/bootstrap/js/bootstrap-datepicker.js"></script>
    <script src="/bootstrap/js/bootstrap-datepicker.zh-CN.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[time='true']").datepicker({
                format: "yyyy-mm-dd",
                language: "zh-CN",
                clearBtn: true,
                todayHighlight: true,
                autoclose: true,
                todayBtn: "linked"
            });
        });
        $(function () {
            $("[name='showImage']").click(function () {
                var processInstanceId = $(this).attr("processInstanceId");
                $("#taskImageLocation").html("<img src=/task/image/"+processInstanceId+">");
            });
        });
    </script>
</head>

<body>
<jsp:include page="/common/top.jsp"></jsp:include>
<div class="container">
<h1>请假-自定义表单</h1>
<br>
<div class="panel panel-default">
    <form action="/taskCustomForm/startProcess" method="post">
    <%--<input type="hidden" name="processKey" value="121212">--%>
    <div class="panel-heading">
        <h3 class="panel-title">请假表单</h3>
    </div>
    <div class="panel-body">
        <table class="table table-striped">
            <tr>
                <td>请假类型:</td>
                <td>
                    <select name="leaveType" class="form-control">
                        <option value="1">病假</option>
                        <option value="2">事假</option>
                        <option value="3">婚假</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>开始时间:</td>
                <td>
                    <input type="text" name="startTime" class="form-control" time="true" readonly/></td>
            </tr>
            <tr>
                <td>结束时间:</td>
                <td><input type="text" name="endTime" class="form-control" time="true" readonly/></td>
            </tr>
            <tr>
                <td>原因:</td>
                <td>
                    <textarea name="reason" class="form-control" rows="3"></textarea>
                </td>
            </tr>
        </table>
    </div>
    <div class="panel-footer">
        <button type="button" class="btn btn-default">取消</button>

        <button type="submit" class="btn btn-success">提交</button>

    </div>
    </form>
</div>
</div>
</body>
</html>
