<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>

<html>
<head>
    <title>已办任务列表-自定义表单</title>
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="/js/jquery.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[name='showImage']").click(function () {
                var processInstanceId = $(this).attr("processInstanceId");
                $("#taskImageLocation").html("<img src=/taskCustomForm/image/"+processInstanceId+">");
            });

            $("[data-target='#callback-confirm-dialog']").click(function () {
                $("#callback-confirm-dialog-label-taskid").html($(this).attr("taskId"));
                $("#callback-confirm-dialog-label-name").html($(this).attr("taskName"));
            });

            $("#callback-confirm-dialog-button-confirm").click(function () {
                var taskId = $("#callback-confirm-dialog-label-taskid").html();

                // 进行撤销操作
                $.get('/taskCustomForm/callBack/' + taskId, function (data) {
                    $('#callback-confirm-dialog').modal('hide');
                    $('#callback-result-dialog').modal('show');
                    //撤销成功
                    if(data==0){
                        $("#callback-result-dialog-label-name").html("撤销成功");
                        $('#callback-result-dialog').delay(3000).modal('hide');
                    //流程已经结束，撤销失败
                    }else if(data==1){
                        $("#callback-result-dialog-label-name").html("流程已经结束，撤销失败");
                    }else{
                        //下一个处理人已经办理，不能撤销
                        $("#callback-result-dialog-label-name").html("下一个处理人已经办理，不能撤销");
                    }
                });
            });

        });
    </script>
</head>

<body>
<jsp:include page="/common/top.jsp"></jsp:include>
<div class="container">
<h1>已办任务列表-自定义表单</h1>
<br>
<table class="table table-striped">
    <tr>
        <th>任务实例ID</th>
        <th>任务名称</th>
        <th>处理时间</th>
        <th>发起人</th>
        <th>操作</th>
    </tr>


    <c:forEach items="${processedTasks}" var="p">
        <tr>
            <td>${p.id }</td>
            <td>${p.name}</td>
            <td>
                <fmt:formatDate value="${p.endTime}" pattern="yyyy-MM-dd HH:mm:ss"/>
            </td>
            <td>
                <c:forEach items="${processInstances}" var="entry">
                    <c:if test="${entry.key eq p.processInstanceId}">
                        <c:out value="${entry.value.startUserId}"/>
                    </c:if>
                </c:forEach>
            </td>
            <td>
                <a class="btn btn-default" href="#" taskId="${p.id}" taskName="${p.name}" name="callBack" data-toggle="modal" data-target="#callback-confirm-dialog">撤销</a>
                <a class="btn btn-default" processInstanceId="${p.processInstanceId}" name="showImage" data-toggle="modal" data-target=".bs-example-modal-sm">显示当前流程位置</a>
            </td>
        </tr>
    </c:forEach>

</table>


<%--用于显示当前节点图片--%>
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="mySmallModalLabel">流程图</h4>
            </div>
            <div class="modal-body" id="taskImageLocation">
            </div>
        </div>
    </div>
</div>

    <%--确认撤销弹框--%>
    <div class="modal fade" id="callback-confirm-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">确定要撤销 <label id="callback-confirm-dialog-label-name"></label> <label id="callback-confirm-dialog-label-taskid" style="display: none"></label>吗</h4>
                </div>
                <div class="modal-body">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="button" class="btn btn-primary" id="callback-confirm-dialog-button-confirm">撤销</button>
                </div>
            </div>
        </div>
    </div>
    <%--撤销结果弹框--%>
    <div class="modal fade" id="callback-result-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><label id="callback-result-dialog-label-name"></label></h4>
                </div>
            </div>
        </div>
    </div>

</div>
</body>
</html>
