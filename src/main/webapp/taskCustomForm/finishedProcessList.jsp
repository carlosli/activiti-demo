<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>

<html>
<head>
    <title>已完成列表-自定义表单</title>
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="/js/jquery.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[name='formContent']").click(function () {
                var $ele = $(this);
                var businessKey = $ele.attr("businessKey");
                $(".modal-body").height("300px");
                //嵌入表单页面
                $("#process-content-iframe").attr("src",'/taskCustomForm/getFinishedTaskForm/'+businessKey);
            });
        });
    </script>

</head>

<body>
<jsp:include page="/common/top.jsp"></jsp:include>
<div class="container">
<h1>已完成列表-自定义表单</h1>
<br>
<table class="table table-striped">
    <tr>
        <th>执行ID</th>
        <th>流程名称</th>
        <th>开始时间</th>
        <th>结束时间</th>
        <th>参数</th>
        <th>发起人</th>
    </tr>


    <c:forEach items="${finishedProcessList}" var="p">
        <tr>
            <td>${p.id }</td>
            <td>
                <c:forEach items="${processDefinitions}" var="entry">
                    <c:if test="${entry.key eq p.processDefinitionId}">
                        <c:out value="${entry.value.name}"/>
                    </c:if>
                </c:forEach>
            </td>
            <td>
                <fmt:formatDate value="${p.startTime}" pattern="yyyy-MM-dd HH:mm:ss"/>
            </td>
            <td>
                <fmt:formatDate value="${p.endTime}" pattern="yyyy-MM-dd HH:mm:ss"/>
            </td>
            <td>
                <a class="btn btn-default" href="#" businessKey="${p.businessKey}" name="formContent" data-toggle="modal" data-target="#process-dialog">查看流程详情</a></td>
            </td>
            <td>${p.startUserId }</td>
        </tr>
    </c:forEach>

</table>
</div>

<div class="modal fade" id="process-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" id="process-title">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title" id="myModalLabel">请假-自定义表单</h2>
            </div>
            <div class="modal-body" id="process-content">
                <iframe id="process-content-iframe" style="width: 100%;height: 100%;" frameborder="no">
                </iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>
