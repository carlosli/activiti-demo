<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html>
<head>
    <title>登录页</title>
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
    <%--<link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">--%>
    <script src="/js/jquery.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <style type="text/css">
        body {
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #f5f5f5;
        }

        .form-signin {
            max-width: 300px;
            padding: 19px 29px 29px;
            margin: 0 auto 20px;
            background-color: #fff;
            border: 1px solid #e5e5e5;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
            -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
            box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
        }

        .form-signin .form-signin-heading,
        .form-signin .checkbox {
            margin-bottom: 10px;
        }

        .form-signin input[type="text"],
        .form-signin input[type="password"] {
            font-size: 16px;
            height: auto;
            margin-bottom: 15px;
            padding: 7px 9px;
        }

    </style>

</head>

<body>
<jsp:include page="/common/top.jsp"></jsp:include>
<br>
<br>
<br>
<br>

<div class="container">
    <c:if test="${not empty param.error}">
        <div class="alert alert-danger"><strong>用户名或密码错误！！！</strong></div>
    </c:if>
    <form class="form-signin" action="/login/logon" method="post">
        <h2 class="form-signin-heading">登录</h2>
        <input type="text" class="input-block-level form-control" placeholder="用户名" name="username">
        <input type="password" class="input-block-level form-control" placeholder="密码" name="password">
        <label class="checkbox">
            <input type="checkbox" value="remember-me"> 记住我
        </label>
        <button class="btn btn-large btn-primary" type="submit">登录</button>
    </form>
</div>


<div class="container">
    <div class="form-signin" action="/login/logon" method="post">
        <p class="text-warning">密码默认为1.</p>
        <table class="table">
            <tr>
                <th>用户名</th>
                <th>职务</th>
            </tr>
            <tr>
                <td>admin</td>
                <td>管理员</td>
            </tr>
            <tr>
                <td>zhangsan</td>
                <td>经理</td>
            </tr>
            <tr>
                <td>zhaosi</td>
                <td>员工</td>
            </tr>
        </table>
    </div>
</div>

</body>
</html>
