<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Activiti Demo</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#about">About</a></li>
                <li class="dropdown">
                    <a id="drop1" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">任务-外置表单 <b class="caret"></b></a>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="/task/toDoProcessList">待办任务</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="/task/runningProcessList">已办任务</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="/task/finishedProcessList">已完成任务</a></li>
                        <li role="presentation" class="divider"></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="/task/processList">发起任务</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a id="drop2" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">任务-普通表单 <b class="caret"></b></a>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="drop2">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="/taskCustomForm/toDoProcessList">待办任务</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="/taskCustomForm/processedTaskList">已办任务</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="/taskCustomForm/runningProcessList">已办流程</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="/taskCustomForm/finishedProcessList">已完成任务</a></li>
                        <li role="presentation" class="divider"></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="/taskCustomForm/processList">发起任务</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
