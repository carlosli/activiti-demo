<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html>
<head>
    <title>已完成列表-外置表单</title>
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="/js/jquery.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
</head>

<body>
<jsp:include page="/common/top.jsp"></jsp:include>
<div class="container">
<h1>已完成列表-外置表单</h1>
<br>
<table class="table table-striped">
    <tr>
        <th>执行ID</th>
        <th>流程实例ID</th>
        <th>流程定义ID</th>
        <th>开始时间</th>
        <th>结束时间</th>
        <th>发起人</th>
    </tr>


    <c:forEach items="${finishedProcessList}" var="p">
        <tr>
            <td>${p.id }</td>
            <td>${p.processInstanceId }</td>
            <td>${p.processDefinitionId }</td>
            <td>${p.startTime}</td>
            <td>${p.endTime}</td>
            <td>${p.startUserId }</td>
        </tr>
    </c:forEach>

</table>
</div>
</body>
</html>
