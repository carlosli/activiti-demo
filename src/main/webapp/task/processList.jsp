<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html>
<head>
    <title>流程列表-外置表单</title>
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="/js/jquery.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        //显示流程图
        $(function () {
            $("[name='showImage']").click(function () {
                var processId = $(this).attr("processId");
                $("#taskImageLocation").html("<img src=/task/processDefinitionImage/"+processId+">");
            });
        });
        //启动流程
        $(function() {
            $("[name='startProcess']").click(function(){
                var $ele = $(this);
                var processDefinitionId = $ele.attr("processId");
                // 读取启动时的表单
                $.get('/task/getProcessStartForm/'+processDefinitionId, function(form) {
                    // 将表单内容放置到弹出框中
                    $("#process-content").html(form);
                    $("#process-dialog").wrap("<form class='formkey-form' method='post' />");
                    var $form = $('.formkey-form');
                    $form.attr('action', '/task/startProcessWithForm/' + processDefinitionId);
//                    // 初始化日期组件
//                    $form.find('.datetime').datetimepicker({
//                        stepMinute: 5
//                    });
//                    $form.find('.date').datepicker();
                });
            });
        });
    </script>
</head>

<body>
<jsp:include page="/common/top.jsp"></jsp:include>
<div class="container">
<h1>流程列表-外置表单</h1>
<br>

<c:if test="${not empty param.message}">
<div class="alert alert-success">
    流程启动成功,流程实例id:${param.message}
</div>
</c:if>

<table class="table table-striped">
    <tr>
        <th> 流程名称</th>
        <th> 定义id</th>
        <th> key</th>
        <th> 部署id</th>
        <th> 版本</th>
        <th> 流程图</th>
        <th> 操作</th>
    </tr>
    <c:forEach items="${processList}" var="process">
        <tr>
            <td>${process.name}</td>
            <td>${process.id}</td>
            <td>${process.key}</td>
            <td>${process.deploymentId}</td>
            <td>${process.version}</td>
            <td>
                <a class="btn btn-default" processId="${process.id}" name="showImage" data-toggle="modal" data-target="#img-dialog">显示流程图</a>
            </td>
            <%--<td><a class="btn" href="/task/startProcess?processKey=${process.key}">启动</a></td>--%>
            <%--<td><a class="btn" href="/task/startProcessWithForm/${process.key}">启动</a></td>--%>
            <td><a class="btn btn-default" href="#" processId="${process.id}" name="startProcess" data-toggle="modal" data-target="#process-dialog">启动</a></td>
        </tr>
    </c:forEach>

</table>

<%--用于显示要启动的流程的初始表单--%>
<div class="modal fade" id="process-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" id="process-title">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body" id="process-content">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="submit" class="btn btn-primary">提交</button>
            </div>
        </div>
    </div>
</div>

<%--用于显示当前节点图片--%>
<div class="modal fade" id="img-dialog" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="mySmallModalLabel">流程图</h4>
            </div>
            <div class="modal-body" id="taskImageLocation">
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>
