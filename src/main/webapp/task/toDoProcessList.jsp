<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html>
<head>
    <title>待办任务列表-外置表单</title>
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="/js/jquery.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        $(function () {
            $("[name='dealProcess']").click(function () {
                var $ele = $(this);
                var taskId = $ele.attr("taskId");
                // 读取启动时的表单
                $.get('/task/getTaskForm/' + taskId, function (form) {
                    $("#process-content").html(form);
                    $("#process-dialog").wrap("<form class='formkey-form' method='post' />");
                    var $form = $('.formkey-form');
                    $form.attr('action', '/task/completeTask/' + taskId);
                });
            });
        });
    </script>
</head>

<body>
<jsp:include page="/common/top.jsp"></jsp:include>
<div class="container">
<h1>待办任务列表-外置表单</h1>
<br>
<table class="table table-striped">
    <tr>
        <th> 流程名称</th>
        <th> 任务id</th>
        <th> 描述</th>
        <th> 处理人</th>
        <th> 版本</th>
        <th> 操作</th>
    </tr>


    <c:forEach items="${tasks}" var="task">
        <tr>
            <td>${task.name}</td>
            <td>${task.id}</td>
            <td>${task.description}</td>
            <td>${task.assignee}</td>
            <td>${task.dueDate}</td>
            <td>
                <c:if test="${task.assignee == sessionScope.user.id}">
                    <a class="btn btn-default" href="#" taskId="${task.id}" name="dealProcess" data-toggle="modal" data-target="#process-dialog">处理</a></td>
                </c:if>
                <c:if test="${task.assignee != sessionScope.user.id}">
                    <a class="btn btn-default" href="/task/claimProcess?processInstanceId=${task.id}">获取当前任务</a>
                </c:if>
            </td>
        </tr>
    </c:forEach>
</table>


<div class="modal fade" id="process-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" id="process-title">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body" id="process-content">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="submit" class="btn btn-primary">提交</button>
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>
