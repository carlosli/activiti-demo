package com.lyl.activiti.demo.bpm.graph;

/**
 * 迁移自mossle开源项目
 */
public class GraphElement {
    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
