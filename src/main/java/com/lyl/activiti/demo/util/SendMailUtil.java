package com.lyl.activiti.demo.util;

import org.springframework.context.ApplicationContext;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

/**
 * Created by lyl on 2014/5/13.
 */
public class SendMailUtil {
    public ApplicationContext ctx = null;

    public SendMailUtil(ApplicationContext ctx) {
        this.ctx = ctx;
    }

    public void send(String to, String subject, String text) {
        //获取JavaMailSender bean
        JavaMailSender sender = (JavaMailSender) ctx.getBean("mailSender");
        SimpleMailMessage mail = new SimpleMailMessage(); //<span style="color: #ff0000;">注意SimpleMailMessage只能用来发送text格式的邮件</span>
        try {
            mail.setTo(to);//接受者
            mail.setFrom("longtest@163.com");//发送者,这里还可以另起Email别名，不用和xml里的username一致
            mail.setSubject(subject);//主题
            mail.setText(text);//邮件内容
            sender.send(mail);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}