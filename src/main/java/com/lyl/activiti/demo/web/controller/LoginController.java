package com.lyl.activiti.demo.web.controller;

import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * 登录页面
 *
 * @author lyl
 */
@Controller
@RequestMapping(value = "/login")
public class LoginController extends BaseController{

    @RequestMapping(value = "/login")
    public String login() {
        return "/login";
    }


    @RequestMapping(value = "/logon")
    public String logon(@RequestParam("username") String userName, @RequestParam("password") String password, HttpSession session) {
        System.out.println(userName);
        System.out.println(password);
        boolean result = identityService.checkPassword(userName, password);
        if (result) {
            //获取用户及用户组
            User user = identityService.createUserQuery().userId(userName).singleResult();
            session.setAttribute("user",user);

            List<Group> groups = identityService.createGroupQuery().groupMember(userName).list();
            session.setAttribute("group",groups);
            return "redirect:/task/toDoProcessList";
        }
            return "redirect:/login/login?error=true";
    }


//    @RequestMapping(value = "/logout")
//    public String logout(HttpSession session) {
//        session.removeAttribute("user");
//        return "/login";
//    }

}
