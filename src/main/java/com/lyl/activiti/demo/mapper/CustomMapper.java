package com.lyl.activiti.demo.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;

public interface CustomMapper {

    @Select("select id_ from ACT_HI_ACTINST where task_id_=#{taskId}")
    String selectIdFormActHiActinstByTaskId(String taskId);

    @Select("select task_id_ from ACT_HI_ACTINST where id_=#{id}")
    String selectTaskIdFormActHiActinstById(String id);

    @Delete("delete from ACT_HI_ACTINST where id_=#{id}")
    int deleteFromActHiActinstById(String id);
}