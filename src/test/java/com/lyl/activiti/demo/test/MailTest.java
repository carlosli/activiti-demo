package com.lyl.activiti.demo.test;

import com.lyl.activiti.demo.BaseTest;
import com.lyl.activiti.demo.util.SendMailUtil;
import com.lyl.activiti.demo.util.SpringContextUtil;
import org.junit.Test;

/**
 * Created by liyulong on 2014/5/13.
 */
public class MailTest extends BaseTest {


    @Test
    public void  test(){
        SendMailUtil sendMail = new SendMailUtil(SpringContextUtil.getApplicationContext());
        sendMail.send("longtest","标题","内容");
    }
}
