package com.lyl.activiti.demo.test;

import com.lyl.activiti.demo.BaseTest;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.junit.Test;

import java.util.List;

/**
 * Created by liyulong on 14-3-26.
 */
public class TestActivitiDeployment extends BaseTest{

    @Test
    public void dfdf(){
        List<HistoricTaskInstance> processedTasks = historyService
                .createHistoricTaskInstanceQuery().taskAssignee("admin")
                .list();
        System.out.println(processedTasks.size());
    }
    @Test
    public void deploymentList(){
        List<Deployment> deployments = repositoryService.createDeploymentQuery().list();
        for (int i = 0; i < deployments.size(); i++) {
            Deployment deployment = deployments.get(i);
            System.out.println("id:" + deployment.getId() + "; name:" + deployment.getName());
        }

        ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
        List<ProcessDefinition> processDefinitionList = processDefinitionQuery .orderByProcessDefinitionId().asc().list();

        for (int i = 0; i < processDefinitionList.size(); i++) {
            ProcessDefinition processDefinition = processDefinitionList.get(i);
            System.out.println("definitionId:"+processDefinition.getId()+
                    "; definitionName:"+processDefinition.getName()+
                    "; definitionDeploymentId:"+processDefinition.getDeploymentId());
        }
    }
}
