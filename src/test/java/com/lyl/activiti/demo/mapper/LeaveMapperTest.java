package com.lyl.activiti.demo.mapper;

import com.lyl.activiti.demo.BaseTest;
import com.lyl.activiti.demo.bean.Leave;
import com.lyl.activiti.demo.bean.LeaveExample;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by li on 2014/4/20.
 */
public class LeaveMapperTest extends BaseTest {

    @Autowired
    private LeaveMapper leaveMapper;
    @Test
    public void selectList(){
        LeaveExample example = new LeaveExample();//2306
        List<Leave> leaves = leaveMapper.selectByExample(example);
        for (int i = 0; i < leaves.size(); i++) {
            System.out.println(leaves.get(i).getId());
        }
        System.out.println(leaves.size());
    }

    @Test
    public void insert(){
        Leave leave = new Leave();
        leave.setReason("give me a reason");
        leaveMapper.insert(leave);
        System.out.println(leave.getId());
        selectList();
    }
}
