package com.lyl.activiti.demo.mapper;

import com.lyl.activiti.demo.BaseTest;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.impl.context.Context;
import org.activiti.engine.task.Task;
import org.junit.Test;

import java.util.List;

/**
 * Created by li on 2014/4/20.
 */
public class CustomSqlTest extends BaseTest {

    @Test
    public void testCandidateUserTask(){
        //获取当前用户为候选人的任务
        List<Task> tasks = taskService.createTaskQuery().taskCandidateUser("zhangsan").list();
        for (Task task : tasks) {
            System.out.println(task.getProcessInstanceId());
            System.out.println(task.getId());
            System.out.println(task.getProcessDefinitionId());
            System.out.println("------------------");
        }
        //获取已经分配给当前用户的任务
//        List<Task> tasks1 = taskService.createTaskQuery().taskAssignee("zhangsan").list();
//        tasks.addAll(tasks1);
    }

    /**
     * 获取特定用户参与过并且流程已经结束的流程实例
     */
    @Test
    public void testFinishedProcessWithUserId(){
        List<HistoricProcessInstance> list = historyService.createHistoricProcessInstanceQuery().processDefinitionKey("leaveCustomFormProcess").involvedUser("zhaosi").finished().list();
        System.out.println(list);
    }

    /**
     * 测试手动编写sql语句查询
     */
    @Test
    public void testNativeTaskQuery(){
        String sql = "SELECT * FROM " + managementService.getTableName(Task.class) + " T WHERE T.NAME_ = #{taskName}";
        System.out.println(sql);
        List<Task> tasks = taskService.createNativeTaskQuery()
                .sql(sql)
                .parameter("taskName", "经理审核")
                .list();
        for (Task task : tasks) {
            System.out.println(task.getId());
            System.out.println(task.getName());
        }
        System.out.println(tasks.size());
    }

    /**
     * 测试显示待办任务的同时列出他的上一级任务的办理人 (可按照办理人过滤)
     */
    @Test
    public void testShowLastExecutor(){
        String sql = "select t1.*, t2.ID_,t2.ASSIGNEE_ " +
                "from act_ru_task t1 LEFT JOIN act_hi_taskinst t2 " +
                "on t1.ID_ != t2.ID_ and  t1.PROC_INST_ID_= t2.PROC_INST_ID_ " +
                "GROUP BY t2.PROC_INST_ID_ having t2.ID_ = MAX(t2.ID_) and t2.ASSIGNEE_ = #{assignee}";
        System.out.println(sql);
        List<Task> tasks = taskService.createNativeTaskQuery()
                .sql(sql)
                .parameter("assignee", "zhangsan")
                .list();
        for (Task task : tasks) {
            System.out.println(task.getId());
            System.out.println(task.getName());
        }
        System.out.println(tasks.size());


        //查询符合条件的总数
        String sqlCount = "SELECT COUNT(*) from (select t2.ID_,t2.ASSIGNEE_ " +
                "from act_ru_task t1 LEFT JOIN act_hi_taskinst t2 " +
                "on t1.ID_ != t2.ID_ and  t1.PROC_INST_ID_= t2.PROC_INST_ID_ " +
                "GROUP BY t2.PROC_INST_ID_ having t2.ID_ = MAX(t2.ID_) and t2.ASSIGNEE_ = #{assignee} ) as t3";
        long count = taskService.createNativeTaskQuery().sql(sqlCount).parameter("assignee", "zhangsan").count();
        System.out.println("count:"+count);
    }
}
