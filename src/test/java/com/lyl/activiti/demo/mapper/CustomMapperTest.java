package com.lyl.activiti.demo.mapper;

import com.lyl.activiti.demo.BaseTest;
import com.lyl.activiti.demo.bean.Leave;
import com.lyl.activiti.demo.bean.LeaveExample;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by li on 2014/5/7.
 */
public class CustomMapperTest extends BaseTest {

    @Resource
    CustomMapper customMapper;
    @Test
    public void selectList(){
        System.out.println(customMapper.selectIdFormActHiActinstByTaskId("1"));
    }
    @Test
    public void delete(){
        System.out.println(customMapper.deleteFromActHiActinstById("999"));
    }

}
