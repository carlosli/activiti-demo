package com.lyl.activiti.demo.init;

import com.lyl.activiti.demo.BaseTest;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.junit.Test;

/**
 * 初始化导入用户
 * Created by liyulong on 14-3-25.
 */
public class UserInit extends BaseTest{

    private static final String USER_ADMIN = "admin";
    private static final String User_ZHAOSI = "zhaosi";
    private static final String USER_ZHANGSAN = "zhangsan";
    private static final String GROUP_ADMIN = "admin";
    private static final String GROUP_MANAGER = "manager";
    private static final String GROUP_EMPLOYEE = "employee";

    private static final String DEFAULT_PASSWOED = "1";


    @Test
    public void insertUserAndGroup() {
        makeUser(USER_ADMIN);
        makeUser(USER_ZHANGSAN);
        makeUser(User_ZHAOSI);

        makeGroup(GROUP_ADMIN);
        makeGroup(GROUP_MANAGER);
        makeGroup(GROUP_EMPLOYEE);

        identityService.createMembership(USER_ADMIN,GROUP_ADMIN);
        identityService.createMembership(USER_ZHANGSAN,GROUP_MANAGER);
        identityService.createMembership(User_ZHAOSI,GROUP_EMPLOYEE);

    }

    private User makeUser(String userName) {
        User user = identityService.newUser(userName);
        user.setFirstName(userName);
        user.setPassword(DEFAULT_PASSWOED);
        user.setEmail("longtest@163.com");
        identityService.saveUser(user);
        return user;
    }

    private Group makeGroup(String groupName){
        Group group = identityService.newGroup(groupName);
        identityService.saveGroup(group);
        return group;
    }
}
