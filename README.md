# Activiti demo


## 技术路线

* Activiti 5
* Spring 3
* Mybatis
* Bootstrap
* mysql


## 运行方式

* 执行ant文件 build.xml (用于打包外置表单文件到bar)
* 调整application.properties中的数据库配置
* 执行src\test\resources\business-table.sql进行业务表创建
* 执行com.lyl.activiti.demo.init.UserInit类，进行初始化数据导入

## 注意

* 尚未添加安全管理，请先登陆再操作